# JavaScript Truthy and Falsy Values

In JavaScript, values are either of a primitive type or of an object type. 
- **Primitive** types:
    - `undefined`
    - `null`
    - `boolean`(`true` or `false`)
    - `number`
    - `string`
    - `bigint`
    - `symbol`
- **Object** types (everything apart from primitive type such as): 
    - Array
    - Function
    - Object(`key: value`)
    
Every value also has an inherent **Boolean** value. We can check the boolean value of any value by using the function `Boolean()`.
For instance let's store a **string** value in a variable `answer`, when we `console.log` this variable using the boolean function, we will get `true`. 

```js
let answer = "42"; 
console.log(Boolean(answer)); // -> true
```

This shows that this particular value `"42"` is **truthy**. The counterpart of **truthy** value is **falsy** value. In JavaScript when a value is `false` in a Boolean context, it is known to be a falsy value. These are values that are considered to be falsy:

|Value          |Description       |
|---------------|------------------|
|`false`        |keyword `false`   |
|`0`            |Number `0`        |
|`0n`           |BigInt zero       |
|`"",''` and `` |empty string      |
|`undefined`    |undefined         |
|`null`         |null              |
|`NaN`          |NaN               |
|`-0`           |Negative zero     |
|`document.all` |document.all      |

Beyond these values, every other value is considered to be **truthy**. 


### Type coercion

An important point to consider is in the boolean context, JavaScript uses **type coercion**, this is an **implicit** conversion that JavaScript does to convert a value from one type to another. For instance when we have two numbers; one as a `string` value and the other as a `number` and we use the `+` operator, the numbers will be concatenated and we will get a string as a result.

```js
console.log(4 + "2"); // -> "42"
```

Therefore, we need to be careful while using `==` as `0 == "0"` will result in `true`, but `0` is `false` while `"0"` is `true`. So, to be on the safer side, it is advisable to always use `===` to check for **strict equality** as it checks for the **type** of values as well.

---
### References
- [MDN reference for falsy values](https://developer.mozilla.org/en-US/docs/Glossary/Falsy)
- [For blog structure and topics to cover](https://www.sitepoint.com/javascript-truthy-falsy/)